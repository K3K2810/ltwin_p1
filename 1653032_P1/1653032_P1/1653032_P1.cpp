// 1653032_P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1653032_P1.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HWND hClient;
HWND hFrame;
HWND toolbar;
int* reserveNum = NULL;

int maxcount = 200;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	ChildWndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT    CALLBACK    MDICloseProc(HWND  hChildWnd, LPARAM  lParam){ SendMessage(hClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L); return  1; }
HWND createToolbar(HWND hWnd);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1653032_P1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1653032_P1));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1653032_P1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1653032_P1);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassEx(&wcex);

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= ChildWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 8;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1653032_P1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MY1653032_P1);
	wcex.lpszClassName	= L"DrawChild";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	wcex.cbClsExtra = 8;
	return RegisterClassEx(&wcex);
}


//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   hFrame = hWnd;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}



struct OBJ
{
	int type;
	int x1,x2,y1,y2;
	COLORREF color;

	void input(int intype, int inx1, int iny1, int inx2, int iny2, COLORREF incolor)
	{
		type = intype;
		x1 = inx1;
		x2 = inx2;
		y1 = iny1;
		y2 = iny2;
		color = incolor;
	}

	bool isSelected(int x, int y)
	{
		switch (type)
		{
		case ID_DRAW_LINE:
			if (x > x1 && x < x2 && y < y2 && y > y1)
			{
				return true;
			}
			else return false;
			break;
		case ID_DRAW_RECTANGLE:
			if (x > x1 && x < x2 && y < y2 && y > y1)
			{
				return true;
			}
			return false;
			break;
		case ID_DRAW_ELLIPSE:
			if (x > x1 && x < x2 && y < y2 && y > y1)
			{
				return true;
			}
			return false;
			break;
		}
	};
};


void highlightObj(HWND hWnd, std::vector<OBJ> &object, OBJ tobeHL)
{
	OBJ tobeadd;
	int xver, yver;
	int size = 5;

	xver = tobeHL.x1;
	yver = tobeHL.y1;
	tobeadd.input(ID_DRAW_RECTANGLE, xver - size, yver - size, xver + size, yver + size, RGB(0, 0, 0));
	object.push_back(tobeadd);				
											
	xver = tobeHL.x2;						
	yver = tobeHL.y1;						
	tobeadd.input(ID_DRAW_RECTANGLE, xver - size, yver - size, xver + size, yver + size, RGB(0, 0, 0));
	object.push_back(tobeadd);				
											
	xver = tobeHL.x1;						
	yver = tobeHL.y2;						
	tobeadd.input(ID_DRAW_RECTANGLE, xver - size, yver - size, xver + size, yver + size, RGB(0, 0, 0));
	object.push_back(tobeadd);				
											
	xver = tobeHL.x2;						
	yver = tobeHL.y2;						
	tobeadd.input(ID_DRAW_RECTANGLE, xver - size, yver - size, xver + size, yver + size, RGB(0, 0, 0));
	object.push_back(tobeadd);

	tobeadd.input(tobeHL.type, tobeHL.x1, tobeHL.y1, tobeHL.x2, tobeHL.y2, RGB(0, 0, 255));
	object.push_back(tobeadd);
};


struct DRAW_DATA
{
	std::vector<OBJ> object;
	int tool;
	COLORREF color;
	int wndnum;
	OBJ* select;
	int count;
	HWND handle;
	bool saved;
};

void changeTool(HWND hWnd, int &tool, int newTool)
{
	HMENU menu = GetMenu(hWnd);

	if (newTool == 0)
	{
		CheckMenuItem(menu, tool, MF_UNCHECKED);
		SendMessage(toolbar, TB_CHECKBUTTON, tool, 0);
	}
	CheckMenuItem(menu,tool,MF_UNCHECKED);
	CheckMenuItem(menu,newTool,MF_CHECKED);
	SendMessage(toolbar, TB_CHECKBUTTON, tool, 0);
	SendMessage(toolbar, TB_CHECKBUTTON, newTool, 1);
	tool = newTool;
}

void chooseColor(HWND hWnd,COLORREF &color)
{
	CHOOSECOLOR cc;                 // common dialog box structure 
	static COLORREF acrCustClr[16]; // array of custom colors

// Initialize CHOOSECOLOR 
ZeroMemory(&cc, sizeof(cc));
cc.lStructSize = sizeof(cc);
cc.hwndOwner = hWnd;
cc.lpCustColors = (LPDWORD) acrCustClr;
cc.Flags = CC_FULLOPEN | CC_RGBINIT;

	ChooseColor(&cc);
	color = cc.rgbResult;
}

void Draw(HWND hWnd,HDC hdc, int tool,int x1,int x2,int y1, int y2)
{
	SelectObject(hdc,GetStockObject(NULL_BRUSH));
	switch(tool)
	{
	case ID_DRAW_LINE:
		MoveToEx(hdc,x1,y1,0);
		LineTo(hdc,x2,y2);
		break;
	case ID_DRAW_RECTANGLE:
		Rectangle(hdc,x1,y1,x2,y2);
		break;
	case ID_DRAW_ELLIPSE:
		Ellipse(hdc,x1,y1,x2,y2);
		break;
	}
}

void reDrawObj(HWND hWnd, HDC hdc,int count, std::vector<OBJ> obj)
{
	for(int i = 0; i < count;i++)
		{
			SelectObject(hdc,GetStockObject(DC_PEN));
			SetDCPenColor(hdc,obj[i].color);
			Draw(hWnd,hdc,obj[i].type,obj[i].x1,obj[i].x2,obj[i].y1,obj[i].y2);
		}
}

void reDrawHL(HWND hWnd, HDC hdc, std::vector<OBJ> obj)
{
	for (int i = 0; i < 4; i++)
	{
		SelectObject(hdc, GetStockObject(DC_PEN));
		SetDCPenColor(hdc, obj[i].color);
		Draw(hWnd, hdc, obj[i].type, obj[i].x1, obj[i].x2, obj[i].y1, obj[i].y2);
	}
	HPEN hPen = CreatePen(PS_DASH, 1, RGB(0, 0, 255));
	SelectObject(hdc, hPen);
	Draw(hWnd, hdc, obj[4].type, obj[4].x1, obj[4].x2, obj[4].y1, obj[4].y2);
	DeleteObject(hPen);
}

void createFrame(HWND hWnd)
{
	CLIENTCREATESTRUCT cws;

	cws.hWindowMenu = GetSubMenu(GetMenu(hWnd),2);
	cws.idFirstChild = 50001;

	hClient = CreateWindow(L"MDICLIENT",NULL,WS_CLIPCHILDREN|WS_CHILD,0,0,0,0,hWnd,NULL,hInst,(LPVOID)&cws);
	ShowWindow(hClient,SW_SHOW);
}

HWND createToolbar(HWND hWnd)
{
	HIMAGELIST imagelist = NULL;

	toolbar = CreateWindowEx(0, TOOLBARCLASSNAME, NULL, WS_CHILD | TBSTYLE_FLAT ,0, 0, 0,0, hWnd, NULL, hInst, NULL);

	if (toolbar == NULL)
		return NULL;

	imagelist = ImageList_Create(16, 16, ILC_COLOR16 | ILC_MASK, 12, 0);
	
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONNEW)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONOPEN)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONSAVE)));

	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONCOPY)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONCUT)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONPASTE)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONDELETE)));

	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONLINE)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONRECTANGLE)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONELLIPSE)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONTEXTBOX)));
	ImageList_AddIcon(imagelist, LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICONSELECTOBJECT)));
	
	SendMessage(toolbar, TB_SETIMAGELIST, 0, (LPARAM)imagelist);
	SendMessage(toolbar, TB_LOADIMAGES, (WPARAM)IDB_STD_SMALL_COLOR,(LPARAM)HINST_COMMCTRL);

	int iNew = SendMessage(toolbar, TB_ADDSTRING, (WPARAM)hInst, (LPARAM)IDS_TOOLBAR);

	TBBUTTON button[12] =
	{
		{ 0, ID_FILE_NEW, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew },
		{ 1, ID_FILE_OPEN, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 1 },
		{ 2, ID_FILE_SAVE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 2 },
		{ 3, ID_EDIT_COPY, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 3 },
		{ 4, ID_EDIT_CUT, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 4 },
		{ 5, ID_EDIT_PASTE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 5 },
		{ 6, ID_EDIT_DELETE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 6 },
		{ 7, ID_DRAW_LINE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 7 },
		{ 8, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 8 },
		{ 9, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 9 },
		{ 10, ID_DRAW_TEXT, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 10 },
		{ 11, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, BTNS_AUTOSIZE, { 0 }, 0, iNew + 11 },
	};

	SendMessage(toolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	SendMessage(toolbar, TB_ADDBUTTONS, (WPARAM)12, (LPARAM)&button);


	SendMessage(toolbar, TB_AUTOSIZE, 0, 0);
	
	// Resize the toolbar, and then show it.
	return toolbar;
}

void createDrawWindow(HWND hWnd,int childnum)
{
	WCHAR title[20];
	wsprintf(title,L"Draw window %d",childnum+1);
	MDICREATESTRUCT mdi;
	mdi.hOwner = hInst;
	mdi.style = 0;
	mdi.cx = CW_USEDEFAULT;
	mdi.cy = CW_USEDEFAULT;
	mdi.x = CW_USEDEFAULT;
	mdi.y = CW_USEDEFAULT;
	mdi.szClass = L"DrawChild";
	mdi.szTitle = title;
	mdi.lParam = 0;
	HWND child = (HWND)SendMessage(hClient,WM_MDICREATE,0,(LPARAM)&mdi);

	((DRAW_DATA*)GetWindowLongPtr(child, 0))->wndnum = childnum;
	PostMessage(child, WM_COMMAND, ID_DRAW_LINE, 0);
	SendMessage(hFrame, WM_COMMAND, ID_WINDOW_CASCADE, 0);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static int childnum = 0;

	switch (message)
	{
	case WM_CREATE:
		createFrame(hWnd);
		createToolbar(hWnd);
		ShowWindow(toolbar, TRUE);
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_NEW:
		{
							if (reserveNum == NULL)
							{
								createDrawWindow(hWnd, childnum);
								childnum++;
							}
							else
							{
								createDrawWindow(hWnd, *reserveNum);
								delete reserveNum;
								reserveNum = NULL;
							}
							break;
		}
		case ID_FILE_OPEN:
		{
							 WCHAR filename[200];
							 OPENFILENAME ofn;
							 ZeroMemory(&ofn, sizeof(ofn));
							 ofn.lStructSize = sizeof (ofn);
							 ofn.hwndOwner = NULL;
							 ofn.lpstrFile = filename;
							 ofn.lpstrFile[0] = '\0';
							 ofn.nMaxFile = sizeof(filename);
							 ofn.lpstrFilter = L"Draw\0*.drw\0";
							 ofn.nFilterIndex = 1;
							 ofn.lpstrFileTitle = NULL;
							 ofn.nMaxFileTitle = 0;
							 ofn.lpstrInitialDir = NULL;
							 ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
							 if (GetOpenFileName(&ofn) == 0) break;

							 MDICREATESTRUCT mdi;
							 mdi.hOwner = hInst;
							 mdi.style = 0;
							 mdi.cx = CW_USEDEFAULT;
							 mdi.cy = CW_USEDEFAULT;
							 mdi.x = CW_USEDEFAULT;
							 mdi.y = CW_USEDEFAULT;
							 mdi.szClass = L"DrawChild";
							 mdi.szTitle = ofn.lpstrFile;
							 mdi.lParam = 0;
							 HWND child = (HWND)SendMessage(hClient, WM_MDICREATE, 0, (LPARAM)&mdi);


							 std::fstream instream;
							 instream.open(ofn.lpstrFile, std::ios::in);
							 if (!instream.is_open()) break;

							 instream >> ((DRAW_DATA*)GetWindowLongPtr(child, 0))->tool;
							 instream >> ((DRAW_DATA*)GetWindowLongPtr(child, 0))->color;
							 ((DRAW_DATA*)GetWindowLongPtr(child, 0))->select = NULL;
							 instream >> ((DRAW_DATA*)GetWindowLongPtr(child, 0))->count;
							 ((DRAW_DATA*)GetWindowLongPtr(child, 0))->handle = child;
							 OBJ temp;
							 int x1, x2, y1, y2, type, r, g, b, count = ((DRAW_DATA*)GetWindowLongPtr(child, 0))->count;
							 COLORREF color;
							 for (int i = 0; i <((DRAW_DATA*)GetWindowLongPtr(child, 0))->count; i++)
							 {
								 instream >> type >> x1 >> y1 >> x2 >> y2 >> r >> g >> b;
								 temp.input(type, x1, y1, x2, y2, RGB(r,g,b));
								 ((DRAW_DATA*)GetWindowLongPtr(child, 0))->object.push_back(temp);
							 }
							 ((DRAW_DATA*)GetWindowLongPtr(child, 0))->wndnum = childnum;

							 SendMessage(hFrame, WM_COMMAND, ID_WINDOW_CASCADE, 0);
							 instream.close();
		}
			break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_WINDOW_CASCADE:
			SendMessage(hClient, WM_MDICASCADE, MDITILE_ZORDER, 0);
			break;
		case  ID_WINDOW_CLOSEALL:
			EnumChildWindows(hClient, (WNDENUMPROC)MDICloseProc, 0L);
			childnum = 0;
			break;
		case ID_WINDOW_TILE:
			SendMessage(hClient, WM_MDITILE, MDITILE_HORIZONTAL, 0);
			break;
		default:
            SendMessage((HWND)SendMessage(hClient,WM_MDIGETACTIVE,0,0), WM_COMMAND, wParam, lParam) ;
		}
		break;
	case WM_SIZE:
	{	
		RECT tbrect;
		GetWindowRect(toolbar, &tbrect);
		MoveWindow(hClient, 0,40, LOWORD(lParam), HIWORD(lParam), 1);
		break;
	}
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		RECT clientrect;
		GetClientRect(hWnd, &clientrect);
		MoveWindow(hClient, 0, 40, clientrect.right, clientrect.bottom, 0);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefFrameProc(hWnd,hClient, message, wParam, lParam);
	}
	return DefFrameProc(hWnd,hClient, message, wParam, lParam);
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static int
		x1 = 0, x2 = 0, y1 = 0, y2 = 0, count = 0,editbox = -1,x,y,selected;
	static bool edit = 0,moving = 0;
	static OBJ* obj;
	static int tool;
	static int color;
	static std::vector<OBJ> hlObj;
	static OPENFILENAME ofn;
	static WCHAR filename[200];
	switch (message)
	{
	case WM_CREATE:
		DRAW_DATA* drawdata;
		drawdata = new DRAW_DATA;
		drawdata->tool = ID_DRAW_LINE;
		drawdata->color = RGB(0, 0, 0);
		drawdata->select = NULL;
		drawdata->handle = hWnd;
		drawdata->count = 0;
		drawdata->saved = 0;

		SetLastError(0);
		if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)drawdata) == 0)
		{
			if (GetLastError() != 0)
				int err = 0;
		}
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_EDIT_COPY:
			if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select != NULL)
			{
				int IDRegist = RegisterClipboardFormat(L"OBJ");
				HGLOBAL hgbMem = GlobalAlloc(GHND, sizeof(OBJ));

				OBJ* object = (OBJ*)GlobalLock(hgbMem);
				object->type = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->type;
				object->color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->color;
				object->x1 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->x1;
				object->y1 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->y1;
				object->x2 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->x2;
				object->y2 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->y2;

				if (OpenClipboard(hWnd))
				{
					EmptyClipboard();
					SetClipboardData(IDRegist, hgbMem);
					CloseClipboard();
				}
			}
			break;
		case ID_EDIT_CUT:
			if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select != NULL)
			{
				int IDRegist = RegisterClipboardFormat(L"OBJ");
				HGLOBAL hgbMem = GlobalAlloc(GHND, sizeof(OBJ));

				OBJ* object = (OBJ*)GlobalLock(hgbMem);
				object->type = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->type;
				object->color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->color;
				object->x1 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->x1;
				object->y1 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->y1;
				object->x2 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->x2;
				object->y2 = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select->y2;

				if (OpenClipboard(hWnd))
				{
					EmptyClipboard();
					SetClipboardData(IDRegist, hgbMem);
					CloseClipboard();
				}
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.erase(((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.begin() + selected);
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = NULL;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count--;
				selected = -1;
				edit = 0;
				moving = 0;
				hlObj.clear();
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = NULL;
				InvalidateRect(hWnd, 0, 1);
			}
			break;
		case ID_EDIT_PASTE:
		{
							  if (OpenClipboard(hWnd))
							  {
								  int IDRegist = RegisterClipboardFormat(L"OBJ");
								  HANDLE hData = GetClipboardData(IDRegist);
								  if (hData != NULL)
								  {
									  OBJ* data = (OBJ*)GlobalLock(hData);
									  OBJ tobeadd;
									  tobeadd.type = data->type;
									  tobeadd.color = data->color;
									  tobeadd.x1 = data->x1;
									  tobeadd.y1 = data->y1;
									  tobeadd.x2 = data->x2;
									  tobeadd.y2 = data->y2;
									  GlobalUnlock(hData);
									  ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.push_back(tobeadd);
									  ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count++;
									  InvalidateRect(hWnd, 0, 1);
								  }
								  EmptyClipboard();
								  CloseClipboard();
							  }
		}
			break;
		case ID_EDIT_DELETE:
			if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select != NULL)
			{
				
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.erase(((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.begin() + selected);
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = NULL;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count--;
				selected = -1;
				edit = 0;
				moving = 0;
				hlObj.clear();
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = NULL;
				InvalidateRect(hWnd, 0, 1);
			}
			break;
		case ID_FILE_SAVE:
		{
							 if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->saved == 0)
							 {
								 ZeroMemory(&ofn, sizeof(ofn));
								 ofn.lStructSize = sizeof (ofn);
								 ofn.hwndOwner = NULL;
								 ofn.lpstrFile = filename;
								 ofn.lpstrFile[0] = '\0';
								 ofn.nMaxFile = sizeof(filename);
								 ofn.lpstrFilter = L"Draw\0*.drw\0";
								 ofn.nFilterIndex = 1;
								 ofn.lpstrFileTitle = NULL;
								 ofn.nMaxFileTitle = 0;
								 ofn.lpstrInitialDir = NULL;
								 ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
								 if (GetSaveFileName(&ofn) == 0) break;
								 ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->saved = 1;
							 }

							 std::fstream outstream;
							 outstream.open(ofn.lpstrFile, std::ios::out|std::ios::trunc);

							 if (!outstream.is_open()) break;
							 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool << std::endl;
							 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color << std::endl;
							 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count << std::endl;
							 for (int i = 0; i < ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count; i++)
							 {
								 int r = GetRValue(((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].color);
								 int g = GetGValue(((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].color);
								 int b = GetBValue(((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].color);
								 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].type << " ";
								 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].x1 << " ";
								 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].y1 << " ";
								 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].x2 << " ";
								 outstream << ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].y2 << " ";
								 outstream << r << " ";
								 outstream << g << " ";
								 outstream << b << " ";
								 outstream << std::endl;
							 }
							 outstream.close();
							 SetWindowText(hWnd, filename);
							 break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_DRAW_CHOOSECOLORS:
			chooseColor(hWnd, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color);
			break;
		default:
			tool = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool;
			count = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count;
			color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color;
			if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select != NULL)
			{
				if (!hlObj.empty()) hlObj.clear();
				InvalidateRect(hWnd, 0, 1);
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = NULL;
			}
			changeTool(hWnd, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool, wmId);
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		count = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count;
		tool = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool;
		color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color;

		SelectObject(hdc, GetStockObject(DC_PEN));
		SetROP2(hdc, R2_COPYPEN);
		reDrawObj(hWnd, hdc, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object);
		if (!hlObj.empty())
			reDrawHL(hWnd, hdc, hlObj);

		EndPaint(hWnd, &ps);

		break;
	case WM_MDIACTIVATE:
		changeTool(hWnd, tool, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool);
		hlObj.clear();
		if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select != NULL)
		{
			InvalidateRect(hWnd, 0, 1);
			highlightObj(hWnd, hlObj, *((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select);
			InvalidateRect(hWnd, 0, 1);
		}
		count = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count;
		color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color;
		break;
	case WM_LBUTTONDOWN:
		x1 = x2 = LOWORD(lParam);
		y1 = y2 = HIWORD(lParam);
		if (tool == ID_DRAW_SELECTOBJECT)
		{
			if (edit == 1)
			{
				if (hlObj.empty())
				{
					edit = 0;
				}
				else
				{
					for (int i = 0; i < 5; i++)
					{
						if (hlObj[i].isSelected(x1, y1))
						{
							edit = 1;
							editbox = i;
							break;
						}
					}
					if (editbox == -1) edit = 0;
				}
			}
			if (edit == 0)
			{
				for (int i = count - 1; i > -1; i--)
				{
					if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].isSelected(x1, y1))
					{
						((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->select = &((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i];
						hlObj.clear();
						highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i]);
						InvalidateRect(hWnd, 0, 1);
						edit = 1;
						selected = i;
					}
				}
			}
			if (moving == 1)
			{
				if (!((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].isSelected(x1, y1)) moving = 0;
			}
			if (moving == 0)
			{
				for (int i = count - 1; i > -1; i--)
				{
					if (((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i].isSelected(x1, y1))
					{
						hlObj.clear();
						highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[i]);
						InvalidateRect(hWnd, 0, 1);
						moving = 1;
						selected = i;
						break;
					}
				}
				break;
			}
		}
		break;
	case WM_MOUSEMOVE:
		if (tool == ID_DRAW_SELECTOBJECT && wParam == MK_LBUTTON && edit == 1 && editbox != -1)
		{
			hdc = GetDC(hWnd);
			SelectObject(hdc, GetStockObject(DC_PEN));
			SetDCPenColor(hdc, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].color);
			SetROP2(hdc, R2_NOTXORPEN);
			Draw(hWnd, hdc, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].type, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2);

			x = LOWORD(lParam);
			y = HIWORD(lParam);
			if (editbox == 0)
			{
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1 = x;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1 = y;
			}
			if (editbox == 1)
			{
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2 = x;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1 = y;
			}
			if (editbox == 2)
			{
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1 = x;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2 = y;
			}
			if (editbox == 3)
			{
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2 = x;
				((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2 = y;
			}
			if (edit == 1)
			{
				Draw(hWnd, hdc, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].type, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2);
				hlObj.clear();
				highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected]);
			}
			ReleaseDC(hWnd, hdc);
			break;
		}
		if (tool == ID_DRAW_SELECTOBJECT && wParam == MK_LBUTTON && moving == 1)
		{
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);

			x = x2 - x1;
			y = y2 - y1;

			((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1 += x;
			((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1 += y;
			((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2 += x;
			((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2 += y;
			Draw(hWnd, hdc, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].type, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].x2, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y1, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected].y2);
			hlObj.clear();
			highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected]);
			break;
		}
		tool = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool;
		color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color;
		if (tool == ID_DRAW_SELECTOBJECT) break;
		if(wParam == MK_LBUTTON)
		{
			hdc = GetDC(hWnd);
			SelectObject(hdc,GetStockObject(DC_PEN));
			SetDCPenColor(hdc,color);
			SetROP2(hdc,R2_NOTXORPEN);
			Draw(hWnd,hdc,tool,x1,x2,y1,y2);
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			Draw(hWnd,hdc,tool,x1,x2,y1,y2);
			
			ReleaseDC(hWnd,hdc);
		}
		break;
	case WM_LBUTTONUP:
		if (tool == ID_DRAW_SELECTOBJECT && edit == 0) break;
		if (tool == ID_DRAW_SELECTOBJECT && edit == 1 && editbox != -1)
		{
				hlObj.clear();
				highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected]);
				reDrawHL(hWnd, hdc, hlObj);

				editbox = -1;
				InvalidateRect(hWnd, 0, 1);
				break;
		}
		if (tool == ID_DRAW_SELECTOBJECT && moving == 1)
		{
			hlObj.clear();
			highlightObj(hWnd, hlObj, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object[selected]);
			reDrawHL(hWnd, hdc, hlObj);
			 
			InvalidateRect(hWnd, 0, 1);
			break;
		}
		if (tool == ID_DRAW_SELECTOBJECT && edit == 1 && editbox == -1)
		{
			break;
		}
		tool = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool;
		color = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->color;
		
		OBJ tobeadd;
		tobeadd.input(tool, x1, y1, x2, y2, color);
		((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object.push_back(tobeadd);
		((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->count++;
		
		hdc = GetDC(hWnd);
		SetROP2(hdc,R2_COPYPEN);

		reDrawObj(hWnd, hdc, count, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->object);
		SelectObject(hdc,GetStockObject(DC_BRUSH));

		ReleaseDC(hWnd,hdc);
		break;
	case WM_DESTROY:
		reserveNum = new int;
		*reserveNum = ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->wndnum;
		changeTool(hWnd, ((DRAW_DATA*)GetWindowLongPtr(hWnd, 0))->tool, 0);
		DestroyWindow(hWnd);
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
