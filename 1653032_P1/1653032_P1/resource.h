//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1653032_P1.rc
//
#define IDC_MYICON                      2
#define IDD_MY1653032_P1_DIALOG         102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDS_TOOLBAR                     104
#define IDM_EXIT                        105
#define IDI_MY1653032_P1                107
#define IDI_SMALL                       108
#define IDC_MY1653032_P1                109
#define IDR_MAINFRAME                   128
#define IDI_ICONSAVE                    130
#define IDI_ICONOPEN                    131
#define IDI_ICONNEW                     132
#define IDI_ICONELLIPSE                 133
#define IDI_ICONLINE                    134
#define IDI_ICONRECTANGLE               135
#define IDI_ICONSELECTOBJECT            136
#define IDI_ICONTEXTBOX                 137
#define IDI_ICONCUT                     139
#define IDI_ICONPASTE                   140
#define IDI_ICONCOPY                    141
#define IDI_ICON4                       142
#define IDI_ICONDELETE                  142
#define ID_WINDOW_CASCADE               32771
#define ID_WINDOW_TILE                  32772
#define ID_WINDOW_CLOSEALL              32773
#define ID_DRAW_CHOOSECOLORS            32774
#define ID_DRAW_FONT                    32775
#define ID_DRAW_LINE                    32776
#define ID_DRAW_RECTANGLE               32777
#define ID_DRAW_ELLIPSE                 32778
#define ID_DRAW_TEXT                    32779
#define ID_DRAW_SELECTOBJECT            32780
#define ID_FILE_NEW                     32781
#define ID_FILE_OPEN                    32782
#define ID_FILE_SAVE                    32783
#define ID_DRAW_FILL                    32784
#define ID_DRAW_PIXEL                   32785
#define ID_EDIT_CUT                     32786
#define ID_EDIT_COPY                    32787
#define ID_EDIT_PASTE                   32788
#define ID_EDIT_DELETE                  32789
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        143
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
